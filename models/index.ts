import * as Sequelize from "sequelize";
import {UserAttributes, UserFactory, UserInstance} from "./User";

export interface DbInterface {
    sequelize: Sequelize.Sequelize;
    Sequelize: Sequelize.SequelizeStatic;
    User: Sequelize.Model<UserInstance, UserAttributes>;
}

export const createModels = (sequelizeConfig: any): DbInterface => {
    const { database, username, password, params } = sequelizeConfig;
    const sequelize = new Sequelize(database, username, password, params);

    const db: DbInterface = {
        sequelize,
        Sequelize,
        User: UserFactory(sequelize, Sequelize)
    };

    return db;
};