
1. Install [nodejs](https://nodejs.org/en/) (version 7.5+)
2. Run `npm install`
3. Run `npm start`

## API End Points

Upload an image via `localhost:3000/marker/upload`, avatar field. It will return { `maker_id`: `image_link`}

