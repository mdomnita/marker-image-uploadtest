import * as express from 'express'
import * as multer from 'multer'
import * as cors from 'cors'
import * as fs from 'fs'
import * as path from 'path'
import * as bodyParser from 'body-parser'
import { imageFilter } from './utils';
import { createModels } from './models';
import * as session from 'express-session';
import * as bcrypt from 'bcrypt';

const sequelizeConfig = require('./config');
const db = createModels(sequelizeConfig);
db.sequelize.sync();

// setup
const COLLECTION_NAME = 'images'
const UPLOAD_PATH = 'uploads'
const GEOJSON_PATH = path.join(__dirname, '../static/data/proc/markersReg.geojson')

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, `${UPLOAD_PATH}/`)
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}.jpg`)
    }
})
const upload = multer({ storage: storage, fileFilter: imageFilter });

// optional: clean all data before start
// cleanFolder(UPLOAD_PATH);

// app
const app = express();
app.use(session({
    secret: '2C44-4D44-WppQ38S',
    resave: true,
    saveUninitialized: true
}));
app.use(cors());
app.use(express.static('static'))
app.use(express.static(`${UPLOAD_PATH}`))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}));

const saltRounds = 10;

const filePath = path.join(__dirname, '../static/')

const auth = (req, res, next) => {
    if (req.session && req.session.user) {
        db.User.findOne({
            where: {
                name: req.session.user
            }
        }).then(user=>{
            if(user) {
                next();
            }
            else {
                res.redirect('/login');
                // res.status(401).json({
                //     error: 'user should login'
                // })
            }
        })
    }
    else {
        res.redirect('/login');
        // res.status(401).json({
        //     error: 'user should login'
        // })
    }
};

const admin = (req, res, next) => {
    if (req.session && req.session.user) {
        db.User.findOne({
            where: {
                name: req.session.user
            }
        }).then(user=>{
            if(user && user.isAdmin) {
                next();
            }
            else {
                res.status(401).json({
                    error: 'user should login'
                })
            }
        })
    }
    else {
        res.status(401).json({
            error: 'user should login'
        })
    }
};

app.post('/login', (req, res) => {
    const {username, password} = req.body;
    // console.log(username, password);
    db.User.findOne({
        where: {
            name: username
        }
    }).then((user) => {
        if (user) {
            // console.log('user', user);
            bcrypt.compare(password, user.password).then((result) => {
                // console.log('result', result)
                if (result) {
                    req.session.user = user.name;
                    res.status(200).json({
                        'success': 'loggedin',
                        'user': user
                    })
                }
                else {
                    res.status(401).json({
                        error: 'password or username is incorrect'
                    })
                }
            })
        }
        else {
            res.status(401).json({
                error: 'password or username is incorrect'
            })
        }
    })
})

app.post('/signup', (req, res) => {
    const {username, password, isAdmin, isView, isEdit} = req.body;
    db.User.findOne({
        where: {
            name: username
        }
    }).then((user) => {
       if (user) {
           res.status(409).json({
               error: 'username already exists.'
           })
       }
       else {
            console.log('creating user');
            bcrypt.genSalt(saltRounds, (err, salt) => {
               bcrypt.hash(password, salt, (err, hash) => {
                   if (!err) {
                       db.User.create({
                           name: username,
                           password: hash,
                           isAdmin: isAdmin,
                           isEdit: isEdit,
                           isView: isView
                       }).then((user)=> {
                           req.session.user = user.name;
                           res.status(200).json({
                               'success': 'user loggined successfully',
                           })
                       })
                   }
                   else {
                       console.log('user data error');
                       // res.redirect('/signup');
                       res.status(409).json({
                           error: 'user data error.'
                       })
                   }
               });
           });
       }
    });
})

app.get('/logout', (req, res) => {
    req.session = null;
    // req.session.destroy();
    res.sendFile(filePath + 'logout.html');
    // res.redirect('/');
});

app.get('/', (req, res) => {
    res.sendFile(filePath + 'home.html');
});

app.get('/markerUp', auth, (req, res) => {
    res.sendFile(filePath + 'markerUp.html');
});

app.get('/markerUp.html', auth, (req, res) => {
    res.sendFile(filePath + 'markerUp.html');
});

app.get('/login', (req, res) => {
    res.sendFile(filePath + 'login.html');
});

app.get('/signup', (req, res) => {
    res.sendFile(filePath + 'signup.html');
});

app.get('/users', admin, (req, res) => {
    db.User.findAll().then((users)=>{
        res.status(200).json({
            users: users
        })
    })
})

app.post('/user/:username', admin, (req, res) => {
    const {username, isAdmin, isEdit, isView} = req.body;
    db.User.findOne({
        where: {
            name: req.params.username
        }
    }).then((user) => {
        if (user) {
            user.update({
                name: username,
                isAdmin: isAdmin,
                isEdit: isEdit,
                isView: isView
            }).then((user) => {
                if (user) res.status(200).json({
                    success: 'update successfully',
                    user: user
                })
                else res.status(500).json({
                    error: 'can not update'
                })
            })
        }
        else {
            res.status(404).json({
                error: 'can not find the user by username:' + req.params.username
            })
        }
    })
})

app.delete('/user/:username', admin, (req, res) => {
    db.User.findOne({
        where: {
            name: req.params.username
        }
    }).then((user) => {
        if (user) {
            db.User.destroy({
                where: {name: req.params.username}
            })
        }
        else {
            res.status(404).json({
                error: 'can not find the user'
            })
        }
    });
});

app.post('/marker/upload', auth, upload.any(), async (req, res) => {
    try {
        console.log('marker image upload');
        let obj = {}
        let fullUrl = `${req.protocol}://${req.hostname}:${req.connection.localPort}`
        console.log(fullUrl);
        for (let key in req.files) {
            let datum = req.files[key]
            obj[datum.fieldname] = `${fullUrl}/${datum.filename}`
        }
        res.send(obj)
    } catch (err) {
        console.log(err)
        res.status(400).send('Bad Request')
    }
})

app.get('/exists/:imageName', auth, async (req, res) => {
    let imagePath = path.join(__dirname, `../${UPLOAD_PATH}/${req.params.imageName}`)
    fs.stat(imagePath, async (err, stats) => {
        res.json({exists: !err ? stats && stats.isFile(): false})
    })
})

app.post('/marker/:id', auth, async (req, res) => {
    try {
        let markerId = req.params.id
        console.log('update marker '+markerId);
        console.log(req.body);
        const {NOM, CATEGORIE, ADRESSE, OUVERT, Public, Description, Lat, Long} = req.body

// var features = [NOM, CATEGORIE, ADRESSE, OUVERT, Public, Description, Lat, Long];
        console.log(NOM, CATEGORIE, ADRESSE, OUVERT, Public, Description, Lat, Long)

        let rawData = fs.readFileSync(GEOJSON_PATH)
        let data = JSON.parse(rawData.toString())
        let isUpdated: boolean = false

        for (let key in data.features) {
            if (data.features[key].properties.id != markerId) continue
            data.features[key].properties = Object.assign(
                {},
                data.features[key].properties,
                {
                    'NOM': NOM,
                    'CATEGORIE':CATEGORIE,
                    'ADRESSE': ADRESSE,
                    'Description': Description,
                    'OUVERT': OUVERT,
                    'Public': Public,
                    'Lat': Lat,
                    'Long': Long
                })
            isUpdated = true

        }
        if (isUpdated){
            fs.writeFileSync(GEOJSON_PATH, JSON.stringify(data))
            res.status(200).json({success: true})
        }
        else res.status(400).json({error: `can not find the marker_id: ${markerId}`})

    } catch (err) {
        console.log(err)
        res.status(500).json({error: err.stack})
    }
})

app.use(function (err, req, res, next) {
    if (res.headersSent) {
        return next(err)
    }
    if (err.stack.indexOf('Only image files are allowed') >= 0)
        res.status(400).json({error: err.stack})
    else res.status(500).json({error: err.stack})
})

app.listen(3000, function () {
    console.log('listening on port 3000!');
})